package br.dao;

import br.models.Musica;

import java.util.List;

public interface MusicaDao {

    void salvar(Musica musica);

    List<Musica> recuperarPorPlaylist(long playlistId);

    Musica recuperarPorPlaylistIdEMusicaId(long playlist, long musicaId);

    void atualizar(Musica musica);

    void excluir(long musicaId);
}
