package br.dao;

import br.models.Musica;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class MusicaDaoImpl implements MusicaDao {

    @PersistenceContext
    private EntityManager manager;

    @Override
    public void salvar(Musica musica) {
        manager.persist(musica);
    }

    @Override
    public List<Musica> recuperarPorPlaylist(long playlistId) {
        return manager.createQuery("select m from Musica m where m.playlist.id = :playlistId", Musica.class).setParameter("playlistId", playlistId)
                .getResultList();
    }

    @Override
    public Musica recuperarPorPlaylistIdEMusicaId(long playlist, long musicaId) {
        return manager.createQuery("select m from Musica m where m.playlist.id = :playlistId and m.id = :musicaId", Musica.class)
                .setParameter("playlistId", playlist)
                .setParameter("musicaId", musicaId)
                .getSingleResult();
    }

    @Override
    public void atualizar(Musica musica) {
        manager.merge(musica);
    }

    @Override
    public void excluir(long musicaId) {
        manager.remove(manager.getReference(Musica.class, musicaId));
    }
}
