package br.dao;

import br.models.Playlist;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository // Componente responsavel por acesso a dados
public class PlaylistDaoImpl implements PlaylistDao {

    @PersistenceContext
    private EntityManager manager;

    @Override
    public void salvar(Playlist playlist) {
        manager.persist(playlist);
    }

    @Override
    public List<Playlist> recuperar() {
        return manager.createQuery("select p from Playlist p", Playlist.class).getResultList();
    }

    @Override
    public Playlist recuperarPorID(long id) {
        return manager.find(Playlist.class, id);
    }

    @Override
    public void atualizar(Playlist playlist) {
        manager.merge(playlist);
    }

    @Override
    public void excluir(long id) {
        manager.remove(manager.getReference(Playlist.class, id));
    }
}
